# Changelog

All notable changes to this project will be documented in this file.

## [1.0.1] - 2020-05-17

### Fixed

- Fixed missing gtm-script in \<head\>-tag.
