# JPlugin - Google Tag Manager
This plugin integrate the Google Tag Manager in Joomla.

## System Requirements
This Plugin requires Joomla 3.2 or higher.

## Installation
You should use gtm.zip package to install Google Tag Manager backend of joomla.

## General Configuration
This plugin has a simple set of configuration to integrate Google Tag Manager in Joomla. In the configuration of the Plugin you can set your personal Google Tag Manager ID (for example GTM-XXX).

If you have reason to deploy your Tag Manager containers in a way that supports non-secure pages, you can enable this option. Default Value is only use https://.

The Plugin includes Google Tag Manager Script in head of the page:

```html
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-XXX';</script>
```

and noscript in body of the page:

```html
<noscript><iframe src='https://www.googletagmanager.com/ns.html?id=GTM-XXX'height='0' width='0' style='display:none;visibility:hidden'></iframe></noscript>
```