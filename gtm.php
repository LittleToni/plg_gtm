<?php

defined('_JEXEC') or die('Restricted access');

class PlgSystemGTM extends JPlugin
{
    /**
     * Google Tag Manager ID
     *
     * @since   1.0.0
     * @author  tonipatke
     */
    private $id;

    /**
     * Protocol for non-secure pages support.
     *
     * @since   1.0.0
     * @author  tonipatke
     */
    private $protocol;

    /**
     * Google Tag Manager script
     *
     * @since   1.0.0
     * @author  tonipatke
     */
    private $script;

    /**
     * Google Tag Manager noscript
     *
     * @since   1.0.0
     * @author  tonipatke
     */
    private $noscript;

    /**
     * Constructor
     *
     * @param   object  &$subject  The object to observe.
     * @param   array   $config	An optional associative array of configuration settings.
     *
     * @return void
     *
     * @since   1.0.0
     * @author  tonipatke
     */
    public function __construct(&$subject, $config = array())
    {
        parent::__construct($subject, $config);

        $this->setID();
        
        if (isset($this->id)) {
            $this->setProtocol();
            $this->setScript();
            $this->setNoscript();
        }
    }

    /**
     * Get Google Tag Manager ID.
     *
     * @return  string  $id - Google Tag Manager ID.
     *
     * @since   1.0.0
     * @author  tonipatke
     */
    private function getID()
    {
        $id = $this->params->get('gtm-id');

        return $id;
    }

    /**
     * Get Protocol for non-secure pages support.
     *
     * @return  string $protocol - contains "https://" or "//".
     *
     * @since   1.0.0
     * @author  tonipatke
     */
    private function getProtocol()
    {
        $nonSecure = $this->params->get('nonsecure');
        $protocol = 'https://';

        if ($nonSecure == '1') {
            $protocol = '//';
        }

        return $protocol;
    }

    /**
     * Set Google Tag Manager ID.
     *
     * @return void
     *
     * @since   1.0.0
     * @author  tonipatke
     */
    private function setID()
    {
        $id = $this->getID();

        $this->id = $id;
    }

    /**
     * Set protocol for non-secure pages support.
     *
     * @return void
     *
     * @since   1.0.0
     * @author  tonipatke
     */
    private function setProtocol()
    {
        $protocol = $this->getProtocol();

        $this->protocol = $protocol;
    }


    /**
     * Set Google Tag Manager script.
     *
     * @return void
     *
     * @since   1.0.0
     * @author  tonipatke
     */
    private function setScript()
    {
        $script = "(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            '" . $this->protocol . "www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','" . $this->id . "');";

        $this->script = $script;
    }

    /**
     * Set Google Tag Manager noscript.
     *
     * @return void
     *
     * @since   1.0.0
     * @author  tonipatke
     */
    private function setNoscript()
    {
        $noscript = "<noscript><iframe src='" . $this->protocol . "www.googletagmanager.com/ns.html?id=" . $this->id . "'
            height='0' width='0' style='display:none;visibility:hidden'></iframe></noscript>";

        $this->noscript = $noscript;
    }

    /**
     * Listener for the `onBeforeCompileHead` event.
     *
     * @return  void
     */
    public function onBeforeCompileHead()
    {
        // Get joomla application & document data.
        $app = JFactory::getApplication();
        $document = JFactory::getDocument();

        // If client is site add Google Tag Manager script.
        if ($app->isClient('site')) {
            if ($this->id) {
                $document->addScriptDeclaration($this->script);
            }
        }
    }

    /**
     * Listener for the `onAfterRender` event.
     *
     * @return  void
     */
    public function onAfterRender()
    {
        $app = JFactory::getApplication();

        if ($app->isClient('site')) {
            if ($this->id) {
                // Get Body buffer.
                $buffer = $app->getBody();

                // Pattern for closing <head>-tag and opening <body>-tag.
                $closingHead = '</head>';
                $openBody = '/<body[^>]*>/';

                // Find <body>-tag in substring.
                $closingHeadPosition = strpos($buffer, $closingHead);
                $substring = substr($buffer, $closingHeadPosition);

                // Get <body>-tag.
                preg_match($openBody, $substring, $matches);

                // Replace <body>-tag includes the itself and Google Tag Manager <noscript>-tag.
                $replacement = $matches[0] . $this->noscript;
                $start = strpos($substring, '<body');
                $length = strlen($matches[0]);
                $substring = substr_replace($substring, $replacement, $start, $length);
                $buffer = substr_replace($buffer, $substring, $closingHeadPosition);

                // Set edited body buffer.
                $app->setBody($buffer);
            }
        }
    }
}
